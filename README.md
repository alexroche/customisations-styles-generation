This is an SCSS compilation tool using PostCSS and Node Sass.

It provides a small NPM library to include an scss file, compile it, and run it through some postcss processing before minifying it and outputting it to a third directory.

Usage is as follows;

```
let csp = require('@lumeon/customisations-compiler');

//Looks for "custom.scss" file ...
const filename = "custom";
// ... in the scss directory ...
const inputDir = __dirname + "/scss";
// ... and outputs it to custom.css in the customization directory.
const outputDir = __dirname + "/customization";

csp.process(
    inputDir,
    outputDir,
    filename
);
```
