'use strict';
const fs = require('fs');
const sass = require('node-sass');
const postcss = require('postcss');

class Processor {
  postCss(outputDir, filename) {
    var writeLocation = outputDir+"/"+filename+".css";
    var css = fs.readFileSync(writeLocation);

      var processors = [
          require('postcss-preset-env')({
              browsers: 'last 3 versions'
          }),
          require('cssnano')
      ];

    postcss(processors)
      .process(css, { from: writeLocation, to: writeLocation })
      .then(result => {
        fs.writeFile(writeLocation, result.css, () => true)
      });
  }

  renderSass(inputDir, outputDir, filename) {
     var result = sass.renderSync(
      {
        file: inputDir+"/"+filename+".scss",
        outputStyle: "compressed"
      });


       if (!fs.existsSync(outputDir)){
          fs.mkdirSync(outputDir);
        }
        let fileLocation = outputDir+"/"+filename+'.css';
       fs.writeFileSync(fileLocation, result.css);
  }

  process(inputDir, outputDir, filename)
  {
    this.renderSass(inputDir, outputDir, filename);
    console.log("\x1b[36m%s\x1b[0m", "Styles rendered from:")
    console.log("\x1b[35m%s/%s.scss\x1b[35m", inputDir, filename)
    console.log("\x1b[36m%s\x1b[0m", "to");
    console.log("\x1b[35m%s/%s.css\x1b[0m", outputDir, filename);

    this.postCss(outputDir, filename);
    console.log("\x1b[36mOutput CSS re-processed with PostCSS.\x1b[0m");
  }
}

module.exports = new Processor();
