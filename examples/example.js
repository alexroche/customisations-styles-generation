let csp = require('@lumeon/customisations-compiler');

//Looks for "custom.scss" file ...
const filename = "custom";
// ... in the scss directory ...
const inputDir = __dirname + "/scss";
// ... and outputs it to custom.css in the customization directory.
const outputDir = __dirname + "/css";

csp.process(
    inputDir,
    outputDir,
    filename
);
